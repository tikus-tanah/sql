package sql

import (
	"context"
	"database/sql"
	"database/sql/driver"
)

type DB interface {
	DB() *sql.DB
	Begin() (DB, error)
	BeginTx(ctx context.Context, opts *sql.TxOptions) (DB, error)
	Exec(query string, args ...interface{}) (sql.Result, error)
	ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error)
	Prepare(query string) (*sql.Stmt, error)
	PrepareContext(ctx context.Context, query string) (*sql.Stmt, error)
	Query(query string, args ...interface{}) (*sql.Rows, error)
	QueryContext(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error)
	QueryRow(query string, args ...interface{}) *sql.Row
	QueryRowContext(ctx context.Context, query string, args ...interface{}) *sql.Row
	Commit() error
	Rollback() error
	Stmt(stmt *sql.Stmt) *sql.Stmt
	StmtContext(ctx context.Context, stmt *sql.Stmt) *sql.Stmt
}

type db struct {
	db *sql.DB
}

func Open(driverName, dataSourceName string) (DB, error) {
	sqlDb, err := sql.Open(driverName, dataSourceName)
	if err != nil {
		return nil, err
	}

	return &db{db: sqlDb}, nil
}

func OpenDB(c driver.Connector) DB {
	sqlDb := sql.OpenDB(c)

	return &db{db: sqlDb}
}

func (d *db) DB() *sql.DB {
	return d.db
}

func (d *db) Begin() (DB, error) {
	sqlTx, err := d.db.Begin()
	if err != nil {
		return nil, err
	}

	return &tx{db: d.db, tx: sqlTx}, nil
}

func (d *db) BeginTx(ctx context.Context, opts *sql.TxOptions) (DB, error) {
	sqlTx, err := d.db.BeginTx(ctx, opts)
	if err != nil {
		return nil, err
	}

	return &tx{db: d.db, tx: sqlTx}, nil
}

func (d *db) Exec(query string, args ...interface{}) (sql.Result, error) {
	return d.db.Exec(query, args...)
}

func (d *db) ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error) {
	return d.db.ExecContext(ctx, query, args...)
}

func (d *db) Prepare(query string) (*sql.Stmt, error) {
	return d.db.Prepare(query)
}

func (d *db) PrepareContext(ctx context.Context, query string) (*sql.Stmt, error) {
	return d.db.PrepareContext(ctx, query)
}

func (d *db) Query(query string, args ...interface{}) (*sql.Rows, error) {
	return d.db.Query(query, args...)
}

func (d *db) QueryContext(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error) {
	return d.db.QueryContext(ctx, query, args...)
}

func (d *db) QueryRow(query string, args ...interface{}) *sql.Row {
	return d.db.QueryRow(query, args...)
}

func (d *db) QueryRowContext(ctx context.Context, query string, args ...interface{}) *sql.Row {
	return d.db.QueryRowContext(ctx, query, args...)
}

func (d *db) Commit() error {
	return ErrNotTx
}

func (d *db) Rollback() error {
	return ErrNotTx
}

func (d *db) Stmt(stmt *sql.Stmt) *sql.Stmt {
	panic(ErrNotTx.Error())
}

func (d *db) StmtContext(ctx context.Context, stmt *sql.Stmt) *sql.Stmt {
	panic(ErrNotTx)
}
