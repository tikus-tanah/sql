package sql

import (
	"context"
	"database/sql"
	"errors"
)

var (
	ErrAlreadyTx = errors.New("already in transaction")
	ErrNotTx     = errors.New("not a transaction")
)

type tx struct {
	db *sql.DB
	tx *sql.Tx
}

func (t *tx) DB() *sql.DB {
	return t.db
}

func (t *tx) Begin() (DB, error) {
	return nil, ErrAlreadyTx
}

func (t *tx) BeginTx(_ context.Context, _ *sql.TxOptions) (DB, error) {
	return nil, ErrAlreadyTx
}

func (t *tx) Exec(query string, args ...interface{}) (sql.Result, error) {
	return t.tx.Exec(query, args...)
}

func (t *tx) ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error) {
	return t.tx.ExecContext(ctx, query, args...)
}

func (t *tx) Prepare(query string) (*sql.Stmt, error) {
	return t.tx.Prepare(query)
}

func (t *tx) PrepareContext(ctx context.Context, query string) (*sql.Stmt, error) {
	return t.tx.PrepareContext(ctx, query)
}

func (t *tx) Query(query string, args ...interface{}) (*sql.Rows, error) {
	return t.tx.Query(query, args...)
}

func (t *tx) QueryContext(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error) {
	return t.tx.QueryContext(ctx, query)
}

func (t *tx) QueryRow(query string, args ...interface{}) *sql.Row {
	return t.tx.QueryRow(query, args...)
}

func (t *tx) QueryRowContext(ctx context.Context, query string, args ...interface{}) *sql.Row {
	return t.tx.QueryRowContext(ctx, query, args...)
}

func (t *tx) Commit() error {
	return t.tx.Commit()
}

func (t *tx) Rollback() error {
	return t.tx.Rollback()
}

func (t *tx) Stmt(stmt *sql.Stmt) *sql.Stmt {
	return t.tx.Stmt(stmt)
}

func (t *tx) StmtContext(ctx context.Context, stmt *sql.Stmt) *sql.Stmt {
	return t.tx.StmtContext(ctx, stmt)
}
